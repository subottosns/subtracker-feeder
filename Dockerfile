from debian:11

run apt-get update -y
run apt-get upgrade -y

run apt-get install apache2 libapache2-mod-wsgi-py3 -y
run apt-get install python3 python3-pip -y
run pip3 install psycopg2-binary sqlalchemy

copy scripts /var/www/scripts

copy mod-wsgi.conf /etc/apache2/conf-available/mod-wsgi.conf
run a2enconf mod-wsgi

copy start.sh /
run chmod +x start.sh

cmd /start.sh
